package co.coreapplication.entities;

public class Empleado {
	
	private String nombre;
	private String documento;
	private String secuencia;
	private String userName;
	
	public Empleado() {
		
	}
	
	public Empleado(String secuencia) {
		this.secuencia =secuencia;
	}

	public Empleado(String secuencia, String userName) {
		super();
		this.secuencia = secuencia;
		this.userName = userName;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append("secuencia: "+secuencia+", ");
		sb.append("userName: "+userName+", ");
		sb.append("nombre: "+nombre+", ");
		sb.append("documento: "+documento+" ");
		sb.append("}");
		return sb.toString();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	

}
