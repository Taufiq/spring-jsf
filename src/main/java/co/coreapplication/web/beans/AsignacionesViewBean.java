package co.coreapplication.web.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.coreapplication.entities.Empleado;

@Component
@ManagedBean
@ViewScoped
public class AsignacionesViewBean extends BaseBean{
	Logger LOG = LoggerFactory.getLogger(AsignacionesViewBean.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1031404481356681046L;
	
	@Autowired
	private Empleado empleado;
	
	private List<Empleado> empleados;
	
	@Override
	public void init() {
		LOG.debug(" INICIALIZANDO ASIGNACIONES VIEWBEAN");
		setup();	
	}
		
	@PostConstruct
	public void setup() {
		empleados = new ArrayList<Empleado>();
		
		//empleado = new Empleado();
		empleado.setSecuencia("412343");
		empleado.setUserName("jon");
		
		Empleado empleado2 = new Empleado("234", "tatan");
		
		empleados.add(empleado);
		empleados.add(empleado2);
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public List<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}
	
}
