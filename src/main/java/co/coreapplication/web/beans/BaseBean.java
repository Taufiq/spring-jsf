package co.coreapplication.web.beans;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/*
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Injector;
*/


@SuppressWarnings("serial")
public abstract class BaseBean implements Serializable {
	//private Logger LOG = LoggerFactory.getLogger(BaseBean.class);
	//private Injector injector;

	public BaseBean() {
	}
	
	/*
	public Injector getInjector() {
		if (injector == null) {
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			injector = (Injector) servletContext.getAttribute(Injector.class.getName());
		}
		return injector;
	}

	public void setInjector(Injector injector) {
		this.injector = injector;
	}

	*/
	
	/*
	@PostConstruct
	public void startInjection() { 
		LOG.debug("Init Base Bean {}", this.getClass().getName());
		getInjector().injectMembers(this);
		init();
	}
	*/
	
	public abstract void init();

	protected void printMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	
	 protected String conversion(double valor)
	    {
	      Locale.setDefault(Locale.US);
	      DecimalFormat num = new DecimalFormat("#,##0.0000");
	      return num.format(valor);
	    }
	 
	 protected String conversion6decimales(double valor)
	    {
	      Locale.setDefault(Locale.US);
	      DecimalFormat num = new DecimalFormat("##,###,##0.######");
	      return num.format(valor);
	    }
}
