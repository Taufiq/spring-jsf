package co.coreapplication.web.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import co.coreapplication.entities.Empleado;

@Configuration
@ComponentScan("co.coreapplication")
public class AppConfig {
	
	@Bean
	public Empleado empleado() { return new Empleado(); }
	
	

}
